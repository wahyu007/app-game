let btnSubmit = document.getElementById("submit");
let alertInfo = document.getElementById("alert");
alertInfo.classList.add("hide");

btnSubmit.addEventListener('click', () => {
    login();
});


let login = () => {
    let dataUSer = {
        "email" : document.getElementById("email").value,
        "password" : document.getElementById("password").value
    }

    fetch("http://localhost:8080/api/v1/users/login",{
        headers : {
            "content-type" : "application/json; charset=UTF-8"
        },
        method : 'POST',
        body : JSON.stringify(dataUSer)
    })
    .then(res => res.json())
    // .then(data => console.log(data))
    .then(data => {
        if (data.status == "success") {
            console.log("go to user.html");
            window.location.href = "user.html";
        } else {
            console.log("back to login.html");
            alertInfo.classList.remove("hide");
        }})
    .catch(err => console.log(err));
}